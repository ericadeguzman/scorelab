<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, maximum-scale=1">

<title>What's your Online SCORE?</title>
<link rel="icon" href="favicon.png" type="image/png">
<link rel="shortcut icon" href="favicon.ico" type="img/x-icon">

<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,800italic,700italic,600italic,400italic,300italic,800,700,600' rel='stylesheet' type='text/css'>

<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/font-awesome.css" rel="stylesheet" type="text/css">
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css">

<!--[if IE]><style type="text/css">.pie {behavior:url(PIE.htc);}</style><![endif]-->

<script type="text/javascript" src="js/jquery.1.8.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
<script type="text/javascript" src="js/jquery-scrolltofixed.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/jquery.isotope.js"></script>
<script type="text/javascript" src="js/wow.js"></script>
<script type="text/javascript" src="js/classie.js"></script>

<link href="vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
<link href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">	

<!--[if lt IE 9]>
    <script src="js/respond-1.1.0.min.js"></script>
    <script src="js/html5shiv.js"></script>
    <script src="js/html5element.js"></script>
<![endif]-->
</head>
<body>
<div style="overflow:hidden;">
<header class="header" id="header"><!--header-start-->
	<div class="container">
						<div class="col-md-6">
							<p class="online-score">Your Online Score for</p><h1 id="campaign-gather"><?php echo $_GET['domain']; ?></span><input type="hidden" id="total-remarks" value="0"></h1>

							<div class="">Your total Digital Marketing score based on an objective, data-driven set of parameters. Let ScoreLabs do the science for you.</div>
                                <!-- pie chart -->
								<div class="easy-pie main-pie m-t-20" id="total-remarks-dd" data-percent="0">
                                    <div class="percent" id="total-remarks-di">0</div>
                                    <div class="pie-title" id="total-remarks-tt"></div>
                                </div>							
								<!-- pie chart -->
								<div class="m-t-20 stat-list">
									<div class="stat-grade m-t-20"><p>Performance<span id="performance-dis"><i class="fa fa-spin fa-spinner"></i></span></p></div>
									<div class="progress m-b-10 prog-cus">
										<div class="progress-bar progress-bar-success" id="performance-stat" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
									</div>
									<div class="stat-grade m-t-20"><p>Mobile<span id="mobile-dis"><i class="fa fa-spin fa-spinner"></i></span></p></div>
									<div class="progress m-b-10 prog-cus">
										<div class="progress-bar progress-bar-success" id="mobile-stat" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
									</div>									
									<div class="stat-grade m-t-20"><p>SEO<span id="seo-dis"><i class="fa fa-spin fa-spinner"></i></span></p></div>
									<div class="progress m-b-10 prog-cus">
										<div class="progress-bar progress-bar-success" id="seo-stat" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
									</div>								
									<div class="stat-grade m-t-20"><p>Security<span id="security-dis"><i class="fa fa-spin fa-spinner"></i></span></p></div>
									<div class="progress m-b-10 prog-cus">
										<div class="progress-bar progress-bar-success" id="security-stat" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
									</div>									

								</div>										
						</div>
						
							<div class="col-md-6 p-l-30">
								<img class=" text-right wow fadeInUp delay-02s m-t-30" src="img/macbook-pro.png" alt="">
								
								<h4 class="m-t-30">It looks like your site could use improvement. We’ll send you a detailed report on what to fix — you may want to share with your webmaster. </h4>
								
								<button type="button" class="get-btn btn btn-primary btn-lg">Get my Free Report</button>
							</div>						
									
						
						
	</div>
</header><!--header-end-->


<nav class="main-nav-outer" id="test"><!--main-nav-start-->
	<div class="container">
        <ul class="main-nav">
        	<li><a href="#header">Overall</a></li>
            <li><a href="#performance">Performance</a></li>
            <li><a href="#mobile">Mobile</a></li>
            <li><a href="#seo">SEO</a></li>
            <li><a href="#security">Security</a></li>
        </ul>
        <a class="res-nav_click" href="#"><i class="fa-bars"></i></a>
    </div>
</nav><!--main-nav-end-->



<section class="main-section" id="performance"><!--main-section-start-->
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
			<h2>Performance</h2>
			<div class="progress prog-cus m-b-20">
				<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%"></div>
			</div>
			<h6>Your website needs to function like a well-oiled machine. Any performance issues can have an impact on your site's ability to be properly ranked by the search engines.</h6></div>
		</div>

        <div class="row">
			<div class="col-sm-4 wow fadeInLeft delay-05s m-b-25">
				<div class="mini-charts-item bgm-lightgreen">
					<div class="clearfix">
						<div class="chart">
						<img src="img/page-speed.png" alt=""></div>
						<div class="count">
							<small>Page Size</small>
							<h2 id="page_size-performance"><i class="fa fa-spin fa-spinner"></i></h2>
<!--							<p>So fast! So light!</p>-->
						</div>
					</div>
				</div>
				<div class="readmore">
				<p class="m-b-15">Bigger file sizes take longer to load. For best results, each page should be less than 3 megabytes in size.</p>
				<a data-toggle="modal" href="#modalDefault" class="btn btn-sm btn-default">Read More</a></div>
			</div>
			<div class="col-sm-4 wow fadeInDown delay-05s m-b-25">
				<div class="mini-charts-item bgm-red">
					<div class="clearfix">
						<div class="chart">
						<img src="img/page-speed.png" alt=""></div>
						<div class="count">
							<small>Page Speed</small>
							<h2 id="page_speed-performance"><i class="fa fa-spin fa-spinner"></i></h2>
<!--							<p>Poor</p>-->
						</div>
					</div>
				</div>
				<div class="readmore">
				<p class="m-b-15">Faster websites means more conversion. Users will bounce away from your site if they have to wait for the information they need.</p>
				<a data-toggle="modal" href="#modalDefault" class="btn btn-sm btn-default">Read More</a></div>				
			</div>
			<div class="col-sm-4 wow fadeInRight delay-05s m-b-25">
				<div class="mini-charts-item bgm-blue">
					<div class="clearfix">
						<div class="chart">
						<img src="img/page-speed.png" alt=""></div>
						<div class="count">
							<small>Page Request</small>
							<h2 id="page_request-performance"><i class="fa fa-spin fa-spinner"></i></h2>
<!--							<p>So fast! So light!</p>-->
						</div>
					</div>
				</div>
				<div class="readmore">
				<p class="m-b-15">Each file on your server means an additional request to the server. Decrease load times by combining relevant files, making for faster delivery.</p>
				<a data-toggle="modal" href="#modalDefault" class="btn btn-sm btn-default">Read More</a></div>				
			</div>
        </div>
		
		<!-- Modal Default -->							
		<div class="modal fade" id="modalDefault" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Modal title</h4>
					</div>
					<div class="modal-body">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sodales orci ante, sed ornare eros vestibulum ut. Ut accumsan vitae eros sit amet tristique. Nullam scelerisque nunc enim, non dignissim nibh faucibus ullamcorper. Fusce pulvinar libero vel ligula iaculis ullamcorper. Integer dapibus, mi ac tempor varius, purus nibh mattis erat, vitae porta nunc nisi non tellus. Vivamus mollis ante non massa egestas fringilla. Vestibulum egestas consectetur nunc at ultricies. Morbi quis consectetur nunc.</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-link">Save changes</button>
						<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal Default -->
		
		<div class="row">
					<div class="col-sm-3 wow fadeInUp delay-05s">
						<div class="service-list">
							<div class="service-list-col1">
								<img  id="browser_caching-img"  alt="">
							</div>
							<div class="service-list-col2">
								<h3>Browser Caching</h3>
								<p>Some elements will be used over and over again. Optimize by loading those elements from the local memory instead of delivering it again and again.</p>
								<a data-toggle="modal" href="#modalDefault" class="btn btn-sm btn-default m-t-20">Read More</a>
							</div>
						</div>
					</div>
					<div class="col-sm-3 wow fadeInUp delay-05s">
						<div class="service-list">
							<div class="service-list-col1">
								<img  id="page_redirects-img"  alt="">
							</div>
							<div class="service-list-col2">
								<h3>Page Redirects</h3>
								<p>Every redirect is another request. Efficiency is all about maximum output vs minimum effort.</p>
								<a data-toggle="modal" href="#modalDefault" class="btn btn-sm btn-default m-t-20">Read More</a>
							</div>
						</div>
					</div>
					<div class="col-sm-3 wow fadeInUp delay-05s">			
						<div class="service-list">
							<div class="service-list-col1">
								<img  id="compression-img"  alt="">
							</div>
							<div class="service-list-col2">
								<h3>Compression</h3>
								<p>Site code can have many redundancies or repeating lines. Optimize your CSS and JS by streamlining your code.</p>
								<a data-toggle="modal" href="#modalDefault" class="btn btn-sm btn-default m-t-20">Read More</a>
							</div>
						</div>
					</div>
					<div class="col-sm-3 wow fadeInUp delay-05s">	
						<div class="service-list">
							<div class="service-list-col1">
								<img  id="render_blocking-img"  alt="">
							</div>
							<div class="service-list-col2">
								<h3>Render Blocking</h3>
								<p>Load only what they see. Prevent unecessary scripts from running when they are not yet needed.</p>
								<a data-toggle="modal" href="#modalDefault" class="btn btn-sm btn-default m-t-20">Read More</a>
							</div>
						</div>
					</div>		
		</div>		
	</div>
</section><!--main-section-end-->


<section class="main-section alabaster" id="mobile"><!--main-section-start-->
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
			<h2>Mobile</h2>
			<div class="progress prog-cus m-b-20">
				<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%"></div>
			</div>
			<h6>60% of all internet traffic comes from mobile devices. It is imperative that you take advantage of the mobile market or else you miss out on a huge chunk of potential revenue.</h6></div>
		</div>		
		<div class="row">
			<figure class="col-lg-5 col-sm-4 wow fadeInLeft">
            	<img  src="img/iphone.png" alt="">
            </figure>
        	<div class="col-lg-7 col-sm-8 featured-work">            	
				
            	<div class="featured-box">
                	<div class="featured-box-col1 wow fadeInRight delay-02s m-r-25">
                    	<img id="friendliness-img" alt="">
                    </div>	
                	<div class="featured-box-col2 wow fadeInRight delay-02s">
                        <h3>Friendliness</h3>
						    <div class="card ">
                                <div class="card-body card-padding">
                                    <h4 id="friendliness-mobile"><i class="fa fa-spin fa-spinner"></i></h4>
                                </div>
                            </div>
                        <p>Mobile devices have less that 10% the screen real-estate of a desktop computer. Users will immediately abandon websites that are not readable on a mobile device.</p>
                    </div>    
                </div>
                <div class="featured-box">
                	<div class="featured-box-col1 wow fadeInRight delay-04s m-r-25">
                    	<img id="speed-img" alt="">
                    </div>	
                	<div class="featured-box-col2 wow fadeInRight delay-04s">
                        <h3>Speed</h3>
						    <div class="card ">
                                <div class="card-body card-padding">
                                    <h4 id="speed-mobile"><i class="fa fa-spin fa-spinner"></i></h4>
                                </div>
                            </div>
                        <p>Mobile device also tend to operate on networks with less bandwidth like 3G or LTE. Make sure that mobile requests are optimized for using less bandwidth.</p>
                    </div>    
                </div>
            </div>
        </div>
	</div>
</section><!--main-section-end-->


<section class="main-section" id="seo"><!--main-section client-part-start-->
	<div class="container">

		<div class="row">
			<div class="col-md-6 col-md-offset-3">
			<h2>SEO</h2>
			<div class="progress prog-cus m-b-20">
				<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%"></div>
			</div>
			<h6>Search engines need to understand your website. Write content for humans, but optimize for search engines.</h6></div>
		</div>
		
		
        <div class="row m-b-30">

						<div class="col-md-6 service-list wow fadeInLeft delay-05s">
							<div class="service-list-col2">
								<img id="meta_data-img" class="m-b-20" alt="">
								<h3>META DATA</h3>
								<p class="p-10">Robots needs instruction. Meta tags summarize the content of a page into a a single set of keywords which search engines use to parse your data.
</p>
								<a data-toggle="modal" href="#modalDefault" class="btn btn-sm btn-default">Read More</a>
							</div>
						</div>
						<div class="col-md-6 service-list wow fadeInRight delay-05s">
							<div class="service-list-col2">
								<img id="headings-img" class="m-b-20" alt="">
								<h3>HEADINGS</h3>
								<p class="p-10">Structed content is important, and headers are used to segregate your content into a logical, structured flow.</p>
								<a data-toggle="modal" href="#modalDefault" class="btn btn-sm btn-default">Read More</a>
							</div>
						</div>

		</div>	
		<div class="row">	
						<div class="col-md-6 service-list wow fadeInLeft delay-03s">
							<div class="service-list-col2">
								<img id="page_title-img" class="m-b-20" alt="">
								<h3>Page Title</h3>
								<p class="p-10">Describe your content. Let users know what you are talking about with just a couple of lines.</p>
								<a data-toggle="modal" href="#modalDefault" class="btn btn-sm btn-default">Read More</a>
							</div>
						</div>
						<div class="col-md-6 service-list wow fadeInRight delay-03s">
							<div class="service-list-col2">
								<img id="sitemap-img"  class="m-b-20" alt="">
								<h3>SITEMAP</h3>
								<p class="p-10">This is the table of contents of your entire websites. It helps both users and search engines get to what they need faster and more efficiently.</p>
								<a data-toggle="modal" href="#modalDefault" class="btn btn-sm btn-default">Read More</a>
							</div>
						</div>
        </div>
		
    </div>
</section><!--main-section client-part-end-->

<section class="main-section alabaster" id="security"><!--main-section team-start-->
	<div class="container">
        <h2>Security</h2>
        <h6>Take a closer look into our amazing team. We won’t bite.</h6>

		<div class="row">
			<figure class="col-lg-5 col-sm-4 wow fadeInLeft">
            	<img  src="img/iphone2.png" alt="">
            </figure>
        	<div class="col-lg-7 col-sm-8 featured-work">            	
				
            	<div class="featured-box">
                	<div class="featured-box-col1 wow fadeInRight delay-02s m-r-25">
                    	<img id="ssl-img" alt="">
                    </div>	
                	<div class="featured-box-col2 wow fadeInRight delay-02s">
                        <h3>SSL Certificate</h3>
						    <div class="card ">
                                <div class="card-body card-padding">
                                    <h4 id="ssl-security"><i class="fa fa-spin fa-spinner"></i></h4>
                                </div>
                            </div>
                        <p>HTTPS is a ranking signal. This means that search engines give a slight priority to websites utilizing an SSL certificate.</p>
                    </div>    
                </div>
            	
				<div class="featured-box">
                	<div class="featured-box-col1 wow fadeInRight delay-02s m-r-25">
                    	<img id="malware-img" alt="">
                    </div>	
                	<div class="featured-box-col2 wow fadeInRight delay-02s">
                        <h3>Malware</h3>
						    <div class="card ">
                                <div class="card-body card-padding">
                                    <h4 id="malware-security"><i class="fa fa-spin fa-spinner"></i></h4>
                                </div>
                            </div>
                        <p>Malware gets you blacklisted, and a that means your site is marked for everyone as a potential danger. You most definitely do not want to be on a blacklist.</p>
                    </div>    
                </div>
				<div class="featured-box">
                	<div class="featured-box-col1 wow fadeInRight delay-02s m-r-25">
                    	<img id="blacklist-img" alt="">
                    </div>	
                	<div class="featured-box-col2 wow fadeInRight delay-02s">
                        <h3>Blacklist</h3>
						    <div class="card ">
                                <div class="card-body card-padding">
                                    <h4 id="blacklist-security"><i class="fa fa-spin fa-spinner"></i></h4>
                                </div>
                            </div>
                        <p>Malware gets you blacklisted, and a that means your site is marked for everyone as a potential danger. You most definitely do not want to be on a blacklist.</p>
                    </div>    
                </div>				
            </div>
        </div>
        
    </div>
</section><!--main-section team-end-->



<section class="business-talking"><!--business-talking-start-->
	<div class="container">
        <h2>Let’s Talk About Your Online Scores</h2>
    </div>
</section><!--business-talking-end-->

<footer class="footer">
    <div class="container">
        <span class="copyright">Copyright © 2016 | <a href="http://www.scorelabs.com/">ScoreLabs</a> </span>
    </div>
    <!-- 
        All links in the footer should remain intact. 
        Licenseing information is available at: http://bootstraptaste.com/license/
        You can buy this theme without footer links online at: http://bootstraptaste.com/buy/?theme=Knight
    -->
</footer>



<script type="text/javascript">
    $(document).ready(function(e) {
        $('#test').scrollToFixed();
        $('.res-nav_click').click(function(){
            $('.main-nav').slideToggle();
            return false
        });
    });

	function data_gather(field){
		if(field == 'security')
			var fill_in = ['ssl', 'malware', 'blacklist'];
		else if(field == 'seo')
			var fill_in = ['meta_data', 'headings', 'page_title', 'sitemap'];
		else if(field == 'mobile')
			var fill_in = ['friendliness', 'speed'];
		else if(field == 'performance')
			var fill_in = ['page_size', 'page_speed', 'page_request', 'browser_caching', 'page_redirects', 'compression', 'render_blocking'];

		var campaign = $.trim($('#campaign-gather').text());
		if($.trim(field) != ''){
			$.get('scorelab-api/api.php?action=get_'+field+'&website='+campaign+'&api_key=1234', function (resp) {
				var data = jQuery.parseJSON( resp );
				var ctr = 0, percent = 0, total = parseInt($('#total-remarks').val());

				for(var i=0;i<fill_in.length;i++){
					if ($('#'+fill_in[i]+'-'+field).length !== 0)
						$('#'+fill_in[i]+'-'+field).html(data[0][fill_in[i]]['value']['display']);

					if(data[0][fill_in[i]]['status'] == 2){
						$('#'+fill_in[i]+'-'+field).addClass('c-red');
						$('#'+fill_in[i]+'-img').attr('src', 'img/warning.png');
						ctr+=1;
					}else if(data[0][fill_in[i]]['status'] == true){
						$('#'+fill_in[i]+'-'+field).addClass('c-green');
						$('#'+fill_in[i]+'-img').attr('src', 'img/check.png');
						ctr+=1;
					}else{
						$('#'+fill_in[i]+'-'+field).addClass('c-red');
						$('#'+fill_in[i]+'-img').attr('src', 'img/cross.png');
					}

					if(field == 'performance') {
						$('#'+fill_in[i]+'-'+field).removeClass();
						$('#' + fill_in[i] + '-' + field).addClass('c-white');
					}
					percent += parseInt(data[0][fill_in[i]]['value']['weight']);
				}
				total += parseInt(data[1]['total']);
				$('#total-remarks').val(total);
				$('#total-remarks-dd').attr('style', 'data-percent: '+total);
				$('#total-remarks-di').text(total);
				$('#'+field+'-stat').attr('aria-valuenow', percent);
				$('#'+field+'-stat').attr('style', 'width: '+percent+'%');
				$('#'+field+'-dis').text(percent.toFixed(2)+'%');

				var total = parseInt($('#total-remarks').val());
				var stat = '';
				if(total >= 90){
					stat = 'Great';
				}else if(total >= 80 && total <= 89){
					stat = 'Good';
				}else if(total >= 65 && total <= 79){
					stat = 'Needs Improvement';
				}else if(total >= 0 && total <= 64){
					stat = 'Very Poor';
				}
				$('#total-remarks-tt').text(stat);

			});
		}

	}

	$.when(data_gather('performance'), data_gather('mobile'), data_gather('seo'), data_gather('security')).then(function(a1, a2, a3, a4){

	});
</script>

  <script>
    wow = new WOW(
      {
        animateClass: 'animated',
        offset:       100
      }
    );
    wow.init();
 
  </script>


<script type="text/javascript">
	$(window).load(function(){
		
		$('.main-nav li a').bind('click',function(event){
			var $anchor = $(this);
			
			$('html, body').stop().animate({
				scrollTop: $($anchor.attr('href')).offset().top - 102
			}, 1500,'easeInOutExpo');
			/*
			if you don't want to use the easing effects:
			$('html, body').stop().animate({
				scrollTop: $($anchor.attr('href')).offset().top
			}, 1000);
			*/
			event.preventDefault();
		});
	})
</script>

<script type="text/javascript">

$(window).load(function(){
  
  
  var $container = $('.portfolioContainer'),
      $body = $('body'),
      colW = 375,
      columns = null;

  
  $container.isotope({
    // disable window resizing
    resizable: true,
    masonry: {
      columnWidth: colW
    }
  });
  
  $(window).smartresize(function(){
    // check if columns has changed
    var currentColumns = Math.floor( ( $body.width() -30 ) / colW );
    if ( currentColumns !== columns ) {
      // set new column count
      columns = currentColumns;
      // apply width to container manually, then trigger relayout
      $container.width( columns * colW )
        .isotope('reLayout');
    }
    
  }).smartresize(); // trigger resize to set container width
  $('.portfolioFilter a').click(function(){
        $('.portfolioFilter .current').removeClass('current');
        $(this).addClass('current');
 
        var selector = $(this).attr('data-filter');
        $container.isotope({
			
            filter: selector,
         });
         return false;
    });
  
});

</script>


        <!-- Javascript Libraries -->
        
        
        <script src="vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script src="vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
        
        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
            <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
        <![endif]-->
        
        <script src="js/charts.js"></script>
        <script src="js/functions.js"></script>
        <script src="js/demo.js"></script>
</body>
</html>