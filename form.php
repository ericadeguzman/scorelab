<?php


?>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, maximum-scale=1">

    <title>What's your Online SCORE?</title>
    <link rel="icon" href="favicon.png" type="image/png">
    <link rel="shortcut icon" href="favicon.ico" type="img/x-icon">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="js/jquery.1.8.3.min.js"></script>
</head>
<body>

    <div class="col-md-12">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <center><h1>ScoreLab</h1></center>
            <form method="post" id="form">
                <label>Input Domain:</label>
                <input type="text" class="form-control" id="domain" name="domain" required="required">
                <br/>
                <button class="btn-primary" style="width: 100%;" type="button" onclick="javascript: submit_this()">Submit</button>
            </form>
        </div>
        <div class="col-md-3"></div>
    </div>

    <script>
        $(document).ready(function(){
            $("#domain").keyup(function(event){
                if(event.keyCode == 13){
                    submit_this();
                }
            });
        });

        function submit_this(){
            var domain = $('#domain').val();
            if(domain != '')
                location.href = 'index.php?domain='+domain;
        }
    </script>
</body>
</html>

