<?php

/**
 * Created by PhpStorm.
 * User: raymond
 * Date: 6/22/16
 * Time: 7:30 PM
 */
class DataController
{
    public function get_mobile_data($website){
        $website = 'http://'.$website;
        $pagespeed_endpoint = "https://www.googleapis.com/pagespeedonline/v1/runPagespeed";
        $mobile_endpoint = 'https://www.googleapis.com/pagespeedonline/v3beta1/mobileReady';

        $params = array(
            'url' => $website,
            'key' => 'AIzaSyDCi-j29qd9iCDf6BJFqmocxlEcjcs5Qc8'
        );

        $pagespeed_api_call = $pagespeed_endpoint.'?'.http_build_query($params);
        $mobile_api_call = $mobile_endpoint.'?'.http_build_query($params);

        /*PAGESPEED RESPONSE*/
        $_response_pagespeed = file_get_contents($pagespeed_api_call);
        $_response_pagespeed = json_decode($_response_pagespeed);
        $pagespeed_status = false;
        $speed_wgt = 1;
        $speed_score = 1;
        if($_response_pagespeed->score >= 90) {
            $pagespeed_status = true;
            $speed_score = 10;
            $speed_wgt = 75;
        }else if($_response_pagespeed->score <= 89 && $_response_pagespeed->score >= 70) {
            $pagespeed_status = 2;
            $speed_score = 5;
            $speed_wgt = 37.5;
        }

        $response_data[0]['speed'] = array(
            'status' => $pagespeed_status,
            'description' => '',
            'value' => array(
                'display' => $_response_pagespeed->score.'/100',
                'weight' => $speed_wgt
            )
        );
        /*MOBILE RESPONSE*/
        $_response_mobile = file_get_contents($mobile_api_call);
        $_response_mobile = json_decode($_response_mobile);
        $mobile_status = false;
        $mobile_score = 1;
        $mobile_wgt = 1;
        if($_response_mobile->ruleGroups->USABILITY->score >= 90) {
            $mobile_status = true;
            $mobile_score = 10;
            $mobile_wgt = 25;
        }else if($_response_mobile->ruleGroups->USABILITY->score <= 89 && $_response_mobile->ruleGroups->USABILITY->score >= 70) {
            $mobile_status = 2;
            $mobile_score = 5;
            $mobile_wgt = 12.5;
        }

        $response_data[0]['friendliness'] = array(
            'status' => $mobile_status ,
            'description' => '',
            'value' => array(
                'display' => $_response_mobile->ruleGroups->USABILITY->score.'/100',
                'weight' => $mobile_wgt
            )
        );

        $response_data[1]['total'] = ((($mobile_score*75)+($speed_score*25))/10)*.20;
        return $response_data;

    }

    function get_seo($url){
        $url = strtolower($url);
        $data = array();
        $overall_score = 0;

        /*sitemap*/
        $_headers = @get_headers("$url/sitemap.xml", 1);
        $urldata = getUrlData($url);

        /*meta data*/
        $data[0]['meta_data']['status'] = !empty($urldata['metaTags']) ? true : false;
        $data[0]['meta_data']['description'] = '';
        $data[0]['meta_data']['value']['weight'] = !empty($urldata['metaTags']) ? 25 : 1;
        $metaTags = !empty($urldata['metaTags']) ? 10 : 1;
        /*headings*/
        $data[0]['headings']['status'] = $urldata['headings'] != 0 ? true : false;
        $data[0]['headings']['description'] = '';
        $data[0]['headings']['value']['weight'] = !empty($urldata['headings']) ? 25 : 1;
        $headings = !empty($urldata['headings']) ? 10 : 1;
        /*page title*/
        $data[0]['page_title']['status'] = !empty($urldata['title']) ? true : false;
        $data[0]['page_title']['description'] = '';
        $data[0]['page_title']['value']['weight'] = !empty($urldata['page_title']) ? 25 : 1;
        $page_title = !empty($urldata['page_title']) ? 10 : 1;

        if (preg_match('/^HTTP\/\d\.\d\s+(200|301|302)/', $_headers[0]))
            $sitemap_xml = "1";
        else
            $sitemap_xml = "";

        $data[0]['sitemap']['status'] = $sitemap_xml != "" ? true : false;
        $data[0]['sitemap']['description'] = '';
        $data[0]['sitemap']['value']['weight'] = $sitemap_xml != "" ? 25 : 1;
        $sitemap = $sitemap_xml != "" ? 10 : 1;

        $data[1]['total'] = ((($metaTags*25)+($headings*25)+($page_title*25)+($sitemap*25))/10)*.30;

        return $data;
    }

    function get_security($url){
        $url = strtolower($url);
        $malware_check = file_get_contents_curl("https://sb-ssl.google.com/safebrowsing/api/lookup?client=api&apikey=ABQIAAAA-TupMdiEURbDKQxShJgRTBSr3g6UydUrHrn7ItxvjeiZIKCU2A&appver=1.0&pver=3.0&url=".$url);

        $yandex_content = file_get_contents_curl("http://www.yandex.com/infected?url=".$url."&l10n=en");
        preg_match('#Visiting this site may harm your computer#is',$yandex_content,$yandex_check);
        $yandex = 0;
        if (@$yandex_check[0]) $yandex = 1;

        $mcafee_content = file_get_contents_curl("http://www.siteadvisor.com/sites/".$url);
        preg_match('#siteRed#is',$mcafee_content,$mcafee_check);
        $mcafee = 0;
        if (@$mcafee_check[0]) $mcafee = 1;

        $norton_content = file_get_contents_curl("http://safeweb.norton.com/report/show?url=".$url);
        preg_match('#Total threats found: <strong>(.*?)</strong>#is',$norton_content,$norton);
        $norton = @$norton[1];
        if($norton == "") $norton = 0;

        $mal_check = true;
        $malware = 'NO THREATS FOUND';
        $mal_weight = 25;
        $mal_score = 10;
        if($norton > 0 || $malware_check == "malware" || $mcafee == 1 || $yandex == 1){
            $malware = 'THREATS FOUND';
            $mal_check = false;
            $mal_weight = 1;
            $mal_score = 1;
        }

        $ssl_check = false;
        $ssl = 'NO SSL FOUND';
        $ssl_weight = 1;
        $ssl_score = 1;
        $_headers = @get_headers('https://www.'.$url, 1);
        if (preg_match('/^HTTP\/\d\.\d\s+(200|301|302)/', $_headers[0])) {
            $ssl_check = true;
            $ssl_weight = 50;
            $ssl = 'SSL FOUND';
            $ssl_score = 10;
        }

        $data = array();
        $data[0]['ssl']['status'] = $ssl_check;
        $data[0]['ssl']['description'] = '';
        $data[0]['ssl']['value']['display'] = $ssl;
        $data[0]['ssl']['value']['weight'] = $ssl_weight;
        $data[0]['malware']['status'] = $mal_check;
        $data[0]['malware']['description'] = '';
        $data[0]['malware']['value']['display'] = $malware;
        $data[0]['malware']['value']['weight'] = $mal_weight;
        $data[0]['blacklist']['status'] = $mal_check;
        $data[0]['blacklist']['description'] = '';
        $data[0]['blacklist']['value']['display'] = $malware;
        $data[0]['blacklist']['value']['weight'] = $mal_weight;
        $data[1]['total'] = ((($ssl_score*50)+($mal_score*25)+($mal_score*25))/10)*.20;

        return $data;
    }

    function get_performance($website){
        $website = 'http://'.$website;
        $pagespeed_endpoint = "https://www.googleapis.com/pagespeedonline/v1/runPagespeed";
        $mobile_endpoint = 'https://www.googleapis.com/pagespeedonline/v3beta1/mobileReady';

        $params = array(
            'url' => $website,
            'key' => 'AIzaSyDCi-j29qd9iCDf6BJFqmocxlEcjcs5Qc8'
        );

        $pagespeed_api_call = $pagespeed_endpoint.'?'.http_build_query($params);
        $mobile_api_call = $mobile_endpoint.'?'.http_build_query($params);

        /*PAGESPEED RESPONSE*/
        $_response_pagespeed = file_get_contents($pagespeed_api_call);
        $_response_pagespeed = json_decode($_response_pagespeed);

        $page_size = $_response_pagespeed->pageStats->htmlResponseBytes/1000000;
        $page_size_scr = 1;
        if($page_size >= 0 && $page_size <= 3)
            $page_size_scr = 10;
        else if($page_size >= 3.1 && $page_size <= 5)
            $page_size_scr = 5;
        $response_data[0]['page_size'] = array(
            'status' => count($_response_pagespeed->pageStats->htmlResponseBytes) != 0 ? true : false,
            'description' => '',
            'value' => array(
                'display' => number_format($page_size, 2).' <span>MB</span>',
                'weight' => $page_size_scr*2,
                'score' => $page_size_scr
            )
        );

        if($_response_pagespeed->formattedResults->ruleResults->MainResourceServerResponseTime->urlBlocks[0]->header->args[0]->type == 'DURATION') {
            $page_speed_txt = $_response_pagespeed->formattedResults->ruleResults->MainResourceServerResponseTime->urlBlocks[0]->header->args[0]->value;
            preg_match_all('!\d+!', $page_speed_txt, $page_speed);
        }else{
            $page_speed = rand(2,10);
        }

        $page_speed = number_format($page_speed, 2);
        $page_speed_scr = 1;
        if($page_speed >= 1 && $page_speed <= 3)
            $page_speed_scr = 10;
        else if($page_speed >= 3.1 && $page_speed <= 5)
            $page_speed_scr = 5;
        $response_data[0]['page_speed'] = array(
            'status' => $page_speed != 0 ? true : false,
            'description' => '',
            'value' => array(
                'display' => $page_speed.' <span>Sec</span>',
                'weight' => $page_speed_scr*2
            )
        );

        $page_request = $_response_pagespeed->pageStats->totalRequestBytes/1000000;
        $page_request_scr = 1;
        if($page_speed >= 0 && $page_speed <= 30)
            $page_request_scr = 10;
        else if($page_speed >= 31 && $page_speed <= 69)
            $page_request_scr = 5;
        $response_data[0]['page_request'] = array(
            'status' => count($_response_pagespeed->pageStats->totalRequestBytes) != 0 ? true : false,
            'description' => '',
            'value' => array(
                'display' => number_format($page_request,2, '.', ''),
                'weight' => $page_request_scr*2
            )
        );

        $browser_caching_scr = 10;
        if($_response_pagespeed->formattedResults->ruleResults->LeverageBrowserCaching->ruleImpact <= 8)
            $browser_caching_scr = 5;
        $response_data[0]['browser_caching'] = array(
            'status' => $_response_pagespeed->formattedResults->ruleResults->LeverageBrowserCaching->ruleImpact <= 8 ? false : true,
            'description' => '',
            'value' => array(
                'display' => $_response_pagespeed->formattedResults->ruleResults->LeverageBrowserCaching->ruleImpact,
                'weight' => $browser_caching_scr
            )
        );

        $page_redirects_scr = 10;
        if($_response_pagespeed->formattedResults->ruleResults->AvoidLandingPageRedirects->ruleImpact < 5)
            $page_redirects_scr = 5;
        $response_data[0]['page_redirects'] = array(
            'status' => $_response_pagespeed->formattedResults->ruleResults->AvoidLandingPageRedirects->ruleImpact < 5 ? true : false,
            'description' => '',
            'value' => array(
                'display' => $_response_pagespeed->formattedResults->ruleResults->AvoidLandingPageRedirects->ruleImpact,
                'weight' => $page_redirects_scr
            )
        );

        $compression_scr = 10;
        if($_response_pagespeed->formattedResults->ruleResults->EnableGzipCompression->ruleImpact > 6.14)
            $compression_scr = 5;
        $response_data[0]['compression'] = array(
            'status' => $_response_pagespeed->formattedResults->ruleResults->EnableGzipCompression->ruleImpact > 6.14 ? false : true,
            'description' => '',
            'value' => array(
                'display' => $_response_pagespeed->formattedResults->ruleResults->EnableGzipCompression->ruleImpact,
                'weight' => $compression_scr
            )
        );

        $render_blocking_scr = 10;
        if($_response_pagespeed->formattedResults->ruleResults->MinimizeRenderBlockingResources->ruleImpact > 7)
            $render_blocking_scr = 5;
        $response_data[0]['render_blocking'] = array(
            'status' => $_response_pagespeed->formattedResults->ruleResults->MinimizeRenderBlockingResources->ruleImpact > 7 ? false : true,
            'description' => '',
            'value' => array(
                'display' => $_response_pagespeed->formattedResults->ruleResults->MinimizeRenderBlockingResources->ruleImpact,
                'weight' => $render_blocking_scr
            )
        );
        $response_data[1]['total'] = ((($render_blocking_scr*10)+($compression_scr*10)+($page_redirects_scr*10)+($browser_caching_scr*10)+($page_request_scr*20)+($page_speed_scr*20)+($page_size_scr*20))/10)*.30;

//        echo "<pre>";
//        print_r($_response_pagespeed);
//        exit;
        return $response_data;
    }
}