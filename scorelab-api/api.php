<?php
/**
 * Created by PhpStorm.
 * User: Lea
 * Date: 6/22/2016
 * Time: 12:17 PM
 */


require_once('Controller.php');
require_once('functions.php');
require_once('DataController.php');
if(isset($_GET['api_key'])){
    $_POST = $_GET;
}

if(!isset($_POST['action']) || !isset($_POST['api_key'])){
    $return_data = array(
        "status" => "failed",
        "message" => "Something went wrong! Go ahead and panic!."
    );

    echo json_encode($return_data);
    exit;
}

$data = $_POST;

checkAccess($data);

/*clean website string*/
$data['website'] = str_replace('http://', '', str_replace('www.', '', $data['website']));

$data_controller = new DataController();
switch ($data['action']) {
    case "get_performance":
        $return_data = $data_controller->get_performance($data['website']);
        echo json_encode($return_data);
        break;
    case "get_seo":
        $return_data = $data_controller->get_seo($data['website']);
        echo json_encode($return_data);
        break;
    case "get_security":
        $return_data = $data_controller->get_security($data['website']);
        echo json_encode($return_data);
        break;
    case "get_mobile":
        $return_data = $data_controller->get_mobile_data($data['website']);
        echo json_encode($return_data);
        break;
}
exit;