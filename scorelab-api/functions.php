<?php


function getUrlContents($url, $maximumRedirections = null, $currentRedirection = 0)
{
    $result = false;
    $contents = $url;

    if (isset($contents) && is_string($contents))
    {
        preg_match_all('/<[\s]*meta[\s]*http-equiv="?REFRESH"?' . '[\s]*content="?[0-9]*;[\s]*URL[\s]*=[\s]*([^>"]*)"?' . '[\s]*[\/]?[\s]*>/si', $contents, $match);

        if (isset($match) && is_array($match) && count($match) == 2 && count($match[1]) == 1)
        {
            if (!isset($maximumRedirections) || $currentRedirection < $maximumRedirections)
            {
                return getUrlContents($match[1][0], $maximumRedirections, ++$currentRedirection);
            }

            $result = false;
        }
        else
        {
            $result = $contents;
        }
    }

    return $contents;
}

function getUrlData($url)
{
    $url = 'http://www.'.$url;
    $result = false;
    $contents = file_get_contents($url);

    if (isset($contents) && is_string($contents))
    {
        $title = '';
        $metaTags = '';
        $headings = 0;

        preg_match('/<title>([^>]*)<\/title>/si', $contents, $match );
        if (isset($match) && is_array($match) && count($match) > 0)
        {
            $title = strip_tags($match[1]);
        }

        preg_match_all('/<[\s]*meta[\s]*name="?' . '([^>"]*)"?[\s]*' .'[lang="]*[^>"]*["]*'.'[\s]*content="?([^>"]*)"?[\s]*[\/]?[\s]*>/si', $contents, $match);
        if (isset($match) && is_array($match) && count($match) == 3)
        {
            $originals = $match[0];
            $names = $match[1];
            $values = $match[2];

            if (count($originals) == count($names) && count($names) == count($values))
            {
                $metaTags = array();

                for ($i=0, $limiti=count($names); $i < $limiti; $i++)
                {
                    $metaname=trim(strtolower($names[$i]));
                    $metaname=str_replace("'",'',$metaname);
                    $metaname=str_replace("/",'',$metaname);
                    $metaTags[$metaname] = array (
                        'html' => htmlentities($originals[$i]),
                        'value' => $values[$i]
                    );
                }
            }
        }
        if(sizeof($metaTags)==0) {
            preg_match_all('/<[\s]*meta[\s]*content="?' . '([^>"]*)"?[\s]*' .'[lang="]*[^>"]*["]*'.'[\s]*name="?([^>"]*)"?[\s]*[\/]?[\s]*>/si', $contents, $match);

            if (isset($match) && is_array($match) && count($match) == 3)
            {
                $originals = $match[0];
                $names = $match[2];
                $values = $match[1];

                if (count($originals) == count($names) && count($names) == count($values))
                {
                    $metaTags = array();

                    for ($i=0, $limiti=count($names); $i < $limiti; $i++)
                    {
                        $metaname=trim(strtolower($names[$i]));
                        $metaname=str_replace("'",'',$metaname);
                        $metaname=str_replace("/",'',$metaname);
                        $metaTags[$metaname] = array (
                            'html' => htmlentities($originals[$i]),
                            'value' => $values[$i]
                        );
                    }
                }
            }
        }

        preg_match_all('/<h([1-6])(.*?)<\/(h[1-6])>/is', $contents, $h_tags);
        foreach ($h_tags[3] as $h) {
            $h = strtolower($h);
            if ($h == "h1" || $h == "h2" || $h == "h3" || $h == "h4" || $h == "h5" || $h == "h6") @$headings++;
        }

        $result = array (
            'title' => $title,
            'metaTags' => $metaTags,
            'headings' => $headings
        );
    }

    return $result;
}


function StopCounter($StartTime)
{
    global $StartTime;
    $time = microtime();
    $time = explode(' ', $time);
    $time = $time[1] + $time[0];
    $Endtime = $time;
    $Total = round($Endtime - $StartTime,4);
    return $Total;
}

function call_post($url, $params){

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
//    curl_setopt($ch, CURLOPT_POST, count($params));
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
    $output = curl_exec ($ch);
    echo "<pre>";
    print_r($output);
    exit;
    $info = curl_getinfo($ch);
    curl_close($ch);

    /*if connection to the api  failed*/
    if($info['http_code'] != 200){
        return json_encode($output);
    }

    $output = json_decode($output);
    $output = $output->data;
    return $output;
}

function file_get_contents_curl($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
    curl_setopt($ch, CURLOPT_URL, $url);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}

function checkAccess($data){
    $api_key = '1234';
    if($api_key != $data['api_key']){
        $return_data = array(
            "status" => "failed",
            "message" => "Invalid API key."
        );

        echo json_encode($return_data);
        exit;
    }
}

//returns true, if domain is availible, false if not
function isDomainAvailable($domain)
{
    //check, if a valid url is provided
    if(!filter_var($domain, FILTER_VALIDATE_URL))
    {
        return false;
    }

    //initialize curl
    $curlInit = curl_init($domain);
    curl_setopt($curlInit,CURLOPT_CONNECTTIMEOUT,10);
    curl_setopt($curlInit,CURLOPT_HEADER,true);
    curl_setopt($curlInit,CURLOPT_NOBODY,true);
    curl_setopt($curlInit,CURLOPT_RETURNTRANSFER,true);

    //get answer
    $response = curl_exec($curlInit);

    curl_close($curlInit);

    if ($response) return true;

    return false;
}

function p($arr){
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
}
function pp($arr){
    p($arr);
    exit;
}